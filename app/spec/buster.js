var config = module.exports;

config["My tests"] = {
    rootPath: "../",
    env: "browser",        // or "node"
    sources: [
		"game/src/main/webapp/static/js/game/init.js"
    ],
    tests: [
        "game/src/test/js/*-test.js"
    ],
	libs: [
		"site/src/main/webapp/static/js/require/init.js",
		"spec/require-config.js"
	],
	extensions: [
        require("buster-assertions"),
        require("buster-amd")
	]
};

