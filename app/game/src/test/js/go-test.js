buster.spec.expose();
var expect = buster.referee.expect;

define(['game/src/main/webapp/static/js/game/init'], function(game) {

    describe("the api", function() {
        before(function() {
            this.board = game(5);
        });

        it("should have a grid", function() {
            expect(this.board.grid).toBeObject();
        });
        
        it("should have a play", function() {
            expect(this.board.play).toBeFunction();
        });
        
        it("should have a size", function() {
            expect(this.board.size).toBeNumber();
        });
        
        it("should have a skip", function() {
            expect(this.board.skip).toBeFunction();
        });

        it("should have a name", function() {
            expect(this.board.name).toBeString();
        });
    });
    
    describe("the rules", function() {
        before(function() {
            this.board = game(5);
        });

        it("should start with empty board", function() {
            for (var i = 0; i < this.board.size; i++) {
                expect(this.board.grid[i]).toBeDefined();
                for (var j = 0; j < this.board.size; j++) {
                    expect(this.board.grid[i][j]).toBeDefined();
                }
            }
        });
        
        it("should play black first, then white and alternate", function() {
            this.board.play(1,1);
            
            expect(this.board.grid[1][1].player.name).toBe("black");
            expect(this.board.grid[1][1].player).not.toBe(this.board.turn);
            
            this.board.play(2,2);
            
            expect(this.board.grid[2][2].player.name).toBe("white");
            expect(this.board.grid[2][2].player).not.toBe(this.board.turn);
            
            this.board.play(3,3);
            
            expect(this.board.grid[3][3].player.name).toBe("black");
            expect(this.board.grid[3][3].player).not.toBe(this.board.turn);
        });
        
        it("should not play on occupied position", function() {
            this.board.play(1,1);
            var previous_player = this.board.grid[1][1].player;
            expect(function() {
                this.board.play(1,1);
            }).toThrow();
            expect(this.board.grid[1][1].player).toBe(previous_player);
        });
        
        it("should end on two skiped turns", function() {
            this.board.play(1,1);
            this.board.skip();
            this.board.skip();
            expect(this.board.turn.name).toBe('none');
            expect(this.board.skip).toThrow();
            expect(this.board.play).toThrow();
        });
        
        // --- --- --- --- 
        //|   |   |   |   |
        // --- --- --- --- 
        //|   |   |   |   |
        // --- --- --- --- 
        //|   |   |   |   |
        // --- --- --- --- 
        //|   |   |   |   |
        // --- --- --- --- 
        //|   |   |   |   |
        // --- --- --- --- 
        
        it("should eat", function() {
            buster.log( this.board.deaths.length );
            
            this.board.play(1,1);
            buster.log( this.board.deaths.length );
            
            this.board.play(1,0);
            buster.log( this.board.deaths.length );
            this.board.skip();
            buster.log( this.board.deaths.length );
            this.board.play(0,1);
            buster.log( this.board.deaths.length );
            this.board.skip();
            buster.log( this.board.deaths.length );
            this.board.play(1,2);
            buster.log( this.board.deaths.length );
            this.board.skip();
            buster.log( this.board.deaths.length );
            this.board.play(2,1);
            buster.log( this.board.deaths.length );
            
            expect(this.board.grid[1][1].player.name).toBe('none');
        });
    });
});

