/*global define */

define(function () {
    "use strict";
    [].__proto__.isArray = true;
    [].__proto__.add = function(e) {
        if (this.length === 0 && this.leave_empty) this.leave_empty();
        this[this.length] = e;
        return this;
    };    
    [].__proto__.merge = function(e) {
        var idx = 0, len;
        if (e.isArray) {
            len = e.length;
            for (; idx < len; idx += 1) {
                this.add(e[idx]);
            }
        } else {
            this.add(e);
        }
        return this;
    };
    [].__proto__.remove = function(entry) {
        var i = this.indexOf(entry);
        if ( i > -1 ) {
            this.slice(i,1);
            if (this.length === 0 && this.became_empty) this.became_empty();  
        }
        return this;
    };
    function static_forEach (func) {
        for (var key in this) {
            if (this.hasOwnProperty(key) && this[key] !== undefined && this[key] !== static_forEach){
                func(this[key]);
            }
        }
    };
    
    var black = {
            color : 1,
            name : 'black',
            occupied : []
        },
        white = {
            color : 2,
            name : 'white',
            occupied : []
        },
        no_player = {
            color : 0,
            name : 'none',
            occupied : []
        };

        black.opponent = white;
        white.opponent = black;
    no_player.opponent = no_player;

    function cell_initialize(grid, x, y, size, death_groups) {
        var cell = grid[y][x];
        cell.value = 0;
        cell.x = x;
        cell.y = y;
        cell.adjacents = {
            up   : y === 0       ? undefined : grid[y - 1][x],
            down : y === size -1 ? undefined : grid[y + 1][x],
            left : x === 0       ? undefined : grid[y][x - 1],
            right: x === size -1 ? undefined : grid[y][x + 1]
        };
        cell.adjacents.forEach = static_forEach;
        // ----- empty cell -----
        cell.player = no_player;
        cell.group = {
            linked : [cell],
            liberties : [].merge(cell.adjacents)
        };
        // ----- ---------- -----
        cell.occupy = function (turn) {
            if (cell.player !== no_player) throw new Error("Trying to playing in an already occupied cell");
            cell.group.linked = [cell];
            cell.player = turn;
        };
        cell.join = function () {
            var nerby_groups = {};
            nerby_groups[white.name] = [];
            nerby_groups[black.name] = [];
            nerby_groups[no_player.name] = [];
            
            cell.adjacents.forEach(function (entry){
                nerby_groups[entry.player.name].merge(entry);
            });
            
            //-- cell group init ------
            cell.group = {
                linked : [cell],
                liberties : []
            };
            death_groups.add(cell.group);
            cell.group.liberties.became_emtpy = function() {
                buster.log("became");
                death_groups.add(cell.group);
            };
            cell.group.liberties.leave_emtpy = function() {
                buster.log("leave");
                death_groups.remove(cell.group);
            };
            //-- -------------- ------
            
            nerby_groups[no_player.name].forEach(function(entry){
                cell.group.liberties.merge(entry);
            });
            //--
            if (nerby_groups[cell.player.name]) nerby_groups[cell.player.name].forEach(function(entry){
                cell.group.linked.merge(entry.group.linked);
                cell.group.liberties.merge(entry.group.liberties);
            });
            cell.group.liberties.remove(cell);
            if (nerby_groups[cell.player.name]) nerby_groups[cell.player.name].forEach(function(entry){
                entry.group = cell.group;
            });
            //--
            if (nerby_groups[cell.player.opponent.name]) nerby_groups[cell.player.opponent.name].forEach(function(entry){
                entry.group.liberties.remove(cell);
            });
        };
        cell.removed = function () {
            
        };
    }
    function make_eat(cell) {
        return function () {

        };
    }
    function buildGrid(size, death_groups) {
        var i, j, grid = [];
        for (j = 0; j < size; j += 1) {
            grid[j] = [];
            for (i = 0; i < size; i += 1) {
                grid[j][i] = {};
            }
        }
        for (j = 0; j < size; j += 1) {
            for (i = 0; i < size; i += 1) {
                cell_initialize(grid, i, j, size, death_groups);
            }
        }
        return grid;
    }

    return function (size) {
        var death_groups = [];
        return {
            name : 'go',
            size : size,
            deaths : death_groups,
            grid : buildGrid(size, death_groups),
            turn : black,
            end  : function () {
                this.play = function () { throw Error("Can't play. Game ended."); };
                this.skip = function () { throw Error("Can't skip. Game ended."); };
                this.turn = no_player;
            },
            play : function (x, y) {
                var cell = this.grid[y][x];
                cell.occupy(this.turn);
                cell.join();
                this.turn = this.turn.opponent;
                this.skip = function () {
                    this.turn = this.turn.opponent;
                    this.skip = this.end;
                };
            },
            skip : function () { }
        };
    };
});


