/*global require, alert, console */
require.config({
    paths: {
        jquery: 'jquery/init'
    }
});

var the_board = {};

require(["jquery", "render.jq/init", "game/init"], function ($, render, game) {
    "use strict";
    the_board = game(17);
    
    function renderize() {
        $("#body").render({
            data: {board: the_board},
            after: function() {
                $("grid").click(function(e) {
                    var pos = $(e.target).data();
                    the_board.play(pos.x, pos.y);
                    renderize();
                });
                $("cell").mouseleave(function(e) {
                    var pos = $(e.target).data(),
                        cell = the_board.grid[pos.y][pos.x];
                    cell.group.liberties.forEach(function(entry){
                        $("#cell_"+entry.x+"_"+entry.y).html("");
                    });
                    cell.group.linked.forEach(function(entry){
                        $("#cell_"+entry.x+"_"+entry.y).html("");
                    });
                });
                $("cell").mouseenter(function(e) {
                    var pos = $(e.target).data(),
                        cell = the_board.grid[pos.y][pos.x];
                    $("info").html(
                        "x: " + pos.x + " y: " +pos.y + "<br/>" +
                        "player: " + cell.player.name
                    );
                    cell.group.liberties.forEach(function(entry){
                        var div = $("#cell_"+entry.x+"_"+entry.y);
                        div.html(div.html() + "x");
                    });
                    cell.group.linked.forEach(function(entry){
                        var div = $("#cell_"+entry.x+"_"+entry.y);
                        div.html(div.html() + "o");
                    });
                    
                });
            }
        });
    }
    $(renderize);
    
});


